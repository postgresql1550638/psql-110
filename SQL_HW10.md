// q1
select country, city from country
inner join city on city.country_id = country.country_id;

// q2
select customer.customer_id, first_name, last_name from customer
right join payment on customer.customer_id = payment.customer_id;

// q3
select customer.customer_id, first_name, last_name from customer
right join rental on customer.customer_id = rental.rental_id;